import React from 'react'
import styled from 'styled-components'
import ReactIframeResizer from 'react-iframe-resizer-super'

const Padder = styled.div`
  margin: 0 2.5rem;
`

const iframeResizerOptions = {
  minHeight: 200,
  autoResize: true,
  sizeHeight: true
}

const GlifoIframe = () => {
  return (
    <Padder>
      <ReactIframeResizer src="https://interactivo.latercera.com/memorial-27f/iframe/index.html?v3" iframeResizerOptions={iframeResizerOptions} iframeResizerEnable={true}></ReactIframeResizer>
    </Padder>
  )
}

export default GlifoIframe