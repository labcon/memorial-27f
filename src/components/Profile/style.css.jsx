import styled from 'styled-components'
import theme from '@/styles/theme'

const StProfile = styled.div`
  display: block;
  margin-top: ${theme.headerHeight};
  max-width: ${theme.normalWidth};
  padding: ${theme.verticalSpacing} 1rem;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  h2{
    margin: ${theme.headerHeight} 0;
  }
  h3{
    margin: ${theme.headerHeight} 0;
  }
  small{
    display: block;
    font-size: 1.3rem;
    color: gray;
  }
  .testimonio,
  .testimonio p{
    font-size: 1.4rem;
  }
  .testimonio p{
    margin-bottom: 1.4rem;
  }
  .familiar{
    font-family: 'Acta Book', serif;
  }
  .link{
    margin-top: 4rem;
    a{
      text-decoration: underline;
    }
  }
`

const StPicture = styled.div`
  max-width: 200px;
  margin-left: auto;
  margin-right: auto;
  padding: 1rem;
`

const StSharing = styled.div`
  color: black;
  span{
    color: black;
  }
  svg{
    fill: black;
  }
`

export { StProfile, StSharing, StPicture }