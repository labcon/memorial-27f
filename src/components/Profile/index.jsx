import React, { useState } from 'react'
import Share from '@/components/Sharer'
import FindLocalImage from '@/components/Image/findlocalimage'
import Img from 'gatsby-image'
import ListadoVictimas from '@/components/ListadoVictimas'

import { StProfile, StSharing, StPicture } from './style.css'

const ImageProfile = ({filename, alt}) => {
  const [image, setImage] = useState(FindLocalImage(filename))

  if (!image) {
    return null
  }

  return (
    <StPicture>
      <Img alt={alt ? alt : ''} sizes={image.node.childImageSharp.image} imgStyle={{
        objectFit: "contain",
        objectPosition: "50% 50%",
      }} />
    </StPicture>
  )
}

const dasherize = str => {
  const map = {
    '-': ' ',
    a: 'á|à|ã|â|À|Á|Ã|Â',
    e: 'é|è|ê|É|È|Ê',
    i: 'í|ì|î|Í|Ì|Î',
    o: 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
    u: 'ú|ù|û|ü|Ú|Ù|Û|Ü',
    c: 'ç|Ç',
    n: 'ñ|Ñ',
  }

  let strlow = str.trim().toLowerCase()

  for (let pattern in map) {
    strlow = strlow.replace(new RegExp(map[pattern], 'g'), pattern)
  }
  return strlow
}

const Profile = ({person}) => {
  const { nombres, apellidos, edad, comuna, causadefallecimiento2, region, testimonio, nombredelfamiliar, fechadenacimiento, fechadedefuncion } = person
  const testimoniohtml = testimonio ? `<p>${testimonio.split('\n').join('</p><p>')}</p>` : null
  // console.log(testimoniohtml)
  return (
    <>
      <StProfile>
        <ImageProfile filename={`${nombres} ${apellidos}.jpg`} />
        <h1>{nombres} {apellidos}</h1>
        <h3>{fechadenacimiento} - {fechadedefuncion}</h3>
        <h3>
          <small>Lugar de fallecimiento:</small>
        {comuna}, {region}</h3>
        <h3>
          <small>Causa:</small>
          {causadefallecimiento2}</h3>
        <StSharing>
          <Share
            socialConfig={{
              twitterHandle: 'latercera',
              config: {
                url: `https://interactivo.latercera.com/memorial-27f/${dasherize(nombres)}-${dasherize(apellidos)}/`,
                title: `Memorial 27F: ${nombres} ${apellidos}`,
                hashtags: ['memorial27F', '27F', 'terremoto']
              }
            }}
          />
        </StSharing>
          {testimonio && (
          <>
            <div className="testimonio" dangerouslySetInnerHTML={{ __html: testimoniohtml }}></div>
            {
              nombredelfamiliar && (
                <h3 className="familiar">{nombredelfamiliar}</h3>
              )
            }
            <StSharing>
              <Share
                socialConfig={{
                  twitterHandle: 'latercera',
                  config: {
                    url: `https://interactivo.latercera.com/memorial-27f/${dasherize(nombres)}-${dasherize(apellidos)}/`,
                    title: `Memorial 27F: ${nombres} ${apellidos}`,
                    hashtags: ['memorial27F', '27F', 'terremoto']
                  }
                }}
              />
            </StSharing>
          </>
        )}

        <p className="link">Si necesitas contactarte para completar esta información, puedes <a href="https://docs.google.com/forms/d/e/1FAIpQLSf-xKwrEh5zUI-I1K5X02ftb1h-8Z_STnVCdDQzdR5XXfxm4g/viewform" target="_blank">ir a este formulario</a></p>

      </StProfile>
      <ListadoVictimas />
    </>
  )
}

export default Profile