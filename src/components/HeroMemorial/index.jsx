import React from 'react'
import {
  StHeroMemorial,
  Content,
  Text
} from './style.css'
import { Link } from 'gatsby'
import LocalImage from '@/components/Image/LocalImage'
import logo27f from '@/images/logo-27f.png'
import laimagen from '@/images/instrucciones-27f.jpg'
import laimagenmob from '@/images/einstrucciones-27f-mobile.jpg'

const HeroMemorial = () => (
  <StHeroMemorial minheight="100vh">
    <Content>
      <div className="wrap">
        <img src={logo27f} />
      </div>

      <Text>
        <p>¿Cuántas vidas se perdieron durante el terremoto y tsunami de 2010? ¿Conocemos los nombres de todas esas personas? Hasta el día de hoy <a href="https://interactivo.latercera.com/27f/cuantos-fallecidos-la-pregunta-sin-respuesta/" target="_blank">no existe una lista oficial</a>, a disposición de las familias afectadas y la opinión pública, para su consulta. </p>

        <p>A una década de esa fatal madrugada de febrero, esta investigación de La Tercera busca visibilizar y homenajear a cada una de las víctimas de aquella catástrofe. Del total de 553 personas que configuran esta lista, contactamos aleatoriamente a distintos familiares para recoger sus testimonios, los que compartimos en este memorial.</p>

        <p>Queremos invitarlos a continuar construyendo con nosotros esta lista que, sabemos, no es definitiva. Pueden consultar el <Link to={`/listado-completo/`}>listado completo</Link>, también ver el <a href=" https://docs.google.com/spreadsheets/d/1yUB5a1zXxNT1AXuVNHFmOFlPkatW_RDd8J8R6QjPd6Q/edit#gid=693658462" target="_blank">listado detallado en Hoja de Cálculo (Excel)</a> y contactarnos a través de <a href="https://docs.google.com/forms/d/e/1FAIpQLSf-xKwrEh5zUI-I1K5X02ftb1h-8Z_STnVCdDQzdR5XXfxm4g/viewform" target="_blank">este formulario</a> para enviarnos sus correcciones y comentarios.</p>

      </Text>

      <img width="100%" className="imginstr desktop" src={laimagen} alt=""/>
      <img width="100%" className="imginstr mobile" src={laimagenmob} alt="" />

    </Content>
  </StHeroMemorial>
)

export default HeroMemorial