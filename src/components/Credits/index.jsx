import React from 'react'

import {
	StCredits
} from './style.css'

const Credits = () => (
  <StCredits>
    <div className="creditsWrapper">
      <h2>Créditos</h2>

      <p>
        <div className="col">
          <span>Coordinación y edición:</span>
        </div>
        <div className="col">
          <strong>Tania Opazo</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Investigación y textos:</span>
        </div>
        <div className="col">
          <strong>Alejandra Olguín</strong>
          <strong>Sophía Marabolí</strong>
          <strong>Alison Vivanco</strong>
          <strong>Valentina Contreras</strong>
          <strong>Ignacio Aguirre</strong>
          <strong>Laura Rivadeneira</strong>
          <strong>Javiera Riquelme</strong>
          <strong>Mirko Curihual</strong>
          <strong>Hilda Oliva</strong>
          <strong>Tania Opazo</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Visualización de datos:</span>
        </div>
        <div className="col">
          <strong>Ignacio Pérez Messina</strong>
          <strong>Eugenio Herrera</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Diseño:</span>
        </div>
        <div className="col">
          <strong>Fabián Andrade</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Frontend:</span>
        </div>
        <div className="col">
          <strong>Álex Acuña Viera</strong>
        </div>
      </p>
    </div>

  </StCredits>
)

export default Credits

