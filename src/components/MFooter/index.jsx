import React from 'react'


import {
	MtPFooter
} from './style.css'

const MFooter = () => (
  <MtPFooter>
    <a href="https://interactivo.latercera.com/27f/" target="_blank" rel="noopener noreferrer">

      <p>Un sistema de emergencias cuestionado. Millones en pérdidas, pueblos completos destruidos y cientos de fallecidos. La Tercera repasa las historias tras el terremoto y tsunami que marcó la década pasada.</p>
      <button>REVISA AQUÍ NUESTRO ESPECIAL</button>
    </a>
  </MtPFooter>
)

export default MFooter

