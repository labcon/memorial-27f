import React from 'react';
import Layout from '@/components/Layout';
import Seo from '@/components/Seo';
import HeroMemorial from '@/components/HeroMemorial';
import Credits from '@/components/Credits';
import GlifoIframe from '@/components/GlifoIframe';
import MFooter from '@/components/MFooter';
import Share from '@/components/Sharer'

import styled from 'styled-components'

const StSharing = styled.div`
  color: black;
  span{
    color: black;
  }
  svg{
    fill: black;
  }

`

class IndexPage extends React.Component {
	render() {
		return (
			<Layout>
				<Seo />
				<HeroMemorial />
				<StSharing>
					<Share
						socialConfig={{
							twitterHandle: 'latercera',
							config: {
								url: `https://interactivo.latercera.com/memorial-27f/`,
								title: `Memorial 27F`,
								hashtags: ['memorial27F', '27F', 'terremoto']
							}
						}}
					/>
				</StSharing>
				<GlifoIframe />
				<MFooter />
				<Credits />
			</Layout>
		);
	}
}

export default IndexPage;
